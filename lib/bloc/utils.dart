abstract interface class Triggered {}

abstract interface class Consumed {}

abstract interface class EmptyStateEvent {}

class TriggerBlankStateEvent implements EmptyStateEvent, Triggered {
  const TriggerBlankStateEvent();
}

class ConsumedBlankStateEvent implements EmptyStateEvent, Consumed {
  const ConsumedBlankStateEvent();
}

const triggered = TriggerBlankStateEvent();

const consumed = ConsumedBlankStateEvent();




extension ImmableListExtensions<T> on List<T> {
  List<T> updateImmutableList(int index, T element) {
    return toList()..[index] = element;
  }

  List<T> addItemToImmutableList(T element) {
    return toList()..add(element);
  }
}