// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'note_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$NoteState {
  List<Note> get notes => throw _privateConstructorUsedError;
  int? get selectedNoteIndex => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  bool get addingNote => throw _privateConstructorUsedError;
  EmptyStateEvent get noteAdded => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $NoteStateCopyWith<NoteState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NoteStateCopyWith<$Res> {
  factory $NoteStateCopyWith(NoteState value, $Res Function(NoteState) then) =
      _$NoteStateCopyWithImpl<$Res, NoteState>;
  @useResult
  $Res call(
      {List<Note> notes,
      int? selectedNoteIndex,
      bool loading,
      bool addingNote,
      EmptyStateEvent noteAdded});
}

/// @nodoc
class _$NoteStateCopyWithImpl<$Res, $Val extends NoteState>
    implements $NoteStateCopyWith<$Res> {
  _$NoteStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notes = null,
    Object? selectedNoteIndex = freezed,
    Object? loading = null,
    Object? addingNote = null,
    Object? noteAdded = null,
  }) {
    return _then(_value.copyWith(
      notes: null == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as List<Note>,
      selectedNoteIndex: freezed == selectedNoteIndex
          ? _value.selectedNoteIndex
          : selectedNoteIndex // ignore: cast_nullable_to_non_nullable
              as int?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      addingNote: null == addingNote
          ? _value.addingNote
          : addingNote // ignore: cast_nullable_to_non_nullable
              as bool,
      noteAdded: null == noteAdded
          ? _value.noteAdded
          : noteAdded // ignore: cast_nullable_to_non_nullable
              as EmptyStateEvent,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$NoteStateImplCopyWith<$Res>
    implements $NoteStateCopyWith<$Res> {
  factory _$$NoteStateImplCopyWith(
          _$NoteStateImpl value, $Res Function(_$NoteStateImpl) then) =
      __$$NoteStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<Note> notes,
      int? selectedNoteIndex,
      bool loading,
      bool addingNote,
      EmptyStateEvent noteAdded});
}

/// @nodoc
class __$$NoteStateImplCopyWithImpl<$Res>
    extends _$NoteStateCopyWithImpl<$Res, _$NoteStateImpl>
    implements _$$NoteStateImplCopyWith<$Res> {
  __$$NoteStateImplCopyWithImpl(
      _$NoteStateImpl _value, $Res Function(_$NoteStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notes = null,
    Object? selectedNoteIndex = freezed,
    Object? loading = null,
    Object? addingNote = null,
    Object? noteAdded = null,
  }) {
    return _then(_$NoteStateImpl(
      notes: null == notes
          ? _value._notes
          : notes // ignore: cast_nullable_to_non_nullable
              as List<Note>,
      selectedNoteIndex: freezed == selectedNoteIndex
          ? _value.selectedNoteIndex
          : selectedNoteIndex // ignore: cast_nullable_to_non_nullable
              as int?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      addingNote: null == addingNote
          ? _value.addingNote
          : addingNote // ignore: cast_nullable_to_non_nullable
              as bool,
      noteAdded: null == noteAdded
          ? _value.noteAdded
          : noteAdded // ignore: cast_nullable_to_non_nullable
              as EmptyStateEvent,
    ));
  }
}

/// @nodoc

class _$NoteStateImpl extends _NoteState {
  const _$NoteStateImpl(
      {final List<Note> notes = const [],
      this.selectedNoteIndex = null,
      this.loading = false,
      this.addingNote = false,
      this.noteAdded = consumed})
      : _notes = notes,
        super._();

  final List<Note> _notes;
  @override
  @JsonKey()
  List<Note> get notes {
    if (_notes is EqualUnmodifiableListView) return _notes;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_notes);
  }

  @override
  @JsonKey()
  final int? selectedNoteIndex;
  @override
  @JsonKey()
  final bool loading;
  @override
  @JsonKey()
  final bool addingNote;
  @override
  @JsonKey()
  final EmptyStateEvent noteAdded;

  @override
  String toString() {
    return 'NoteState(notes: $notes, selectedNoteIndex: $selectedNoteIndex, loading: $loading, addingNote: $addingNote, noteAdded: $noteAdded)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NoteStateImpl &&
            const DeepCollectionEquality().equals(other._notes, _notes) &&
            (identical(other.selectedNoteIndex, selectedNoteIndex) ||
                other.selectedNoteIndex == selectedNoteIndex) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            (identical(other.addingNote, addingNote) ||
                other.addingNote == addingNote) &&
            (identical(other.noteAdded, noteAdded) ||
                other.noteAdded == noteAdded));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_notes),
      selectedNoteIndex,
      loading,
      addingNote,
      noteAdded);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NoteStateImplCopyWith<_$NoteStateImpl> get copyWith =>
      __$$NoteStateImplCopyWithImpl<_$NoteStateImpl>(this, _$identity);
}

abstract class _NoteState extends NoteState {
  const factory _NoteState(
      {final List<Note> notes,
      final int? selectedNoteIndex,
      final bool loading,
      final bool addingNote,
      final EmptyStateEvent noteAdded}) = _$NoteStateImpl;
  const _NoteState._() : super._();

  @override
  List<Note> get notes;
  @override
  int? get selectedNoteIndex;
  @override
  bool get loading;
  @override
  bool get addingNote;
  @override
  EmptyStateEvent get noteAdded;
  @override
  @JsonKey(ignore: true)
  _$$NoteStateImplCopyWith<_$NoteStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
