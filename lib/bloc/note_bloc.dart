import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/bloc/utils.dart';
import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'note_bloc.freezed.dart';

class NoteBloc extends Bloc<NoteEvent, NoteState> {
  NoteBloc() : super(const NoteState(loading: true)) {
    on<NoteEvent>((event, emit) async {
      switch (event) {
        case LoadNotesEvent():
          {
            await Future.delayed(const Duration(seconds: 2), () {
              emit(state.copyWith(
                  loading: false, notes: [const Note("first note", false)]));
            });
          }
        case AddNoteEvent():
          {
            emit(state.copyWith(addingNote: true));
            await Future.delayed(const Duration(seconds: 2), () {
              final newNote = Note(event.content, false);
              emit(state.copyWith(
                  addingNote: false,
                  notes: state.notes.addItemToImmutableList(newNote),
                  noteAdded: triggered));
            });
          }
        case UpdateNoteEvent():
          {
            final loadingNote = Note(state.notes[event.index].content, true);
            emit(state.copyWith(
                notes:
                    state.notes.updateImmutableList(event.index, loadingNote)));

            await Future.delayed(const Duration(seconds: 2), () {
              final updatedNote = Note(event.content, false);
              emit(state.copyWith(
                  notes: state.notes
                      .updateImmutableList(event.index, updatedNote)));
            });
          }
        case EditNoteEvent():
          {
            emit(state.copyWith(selectedNoteIndex: event.index));
          }
        case ConsumeAddingNoteEvent():
          {
            emit(state.copyWith(addingNote: false, noteAdded: consumed));
          }
      }
    });
  }
}

//Note events
sealed class NoteEvent {
  const NoteEvent();
}

class LoadNotesEvent extends NoteEvent {
  const LoadNotesEvent();
}

class AddNoteEvent extends NoteEvent {
  final String content;

  const AddNoteEvent(this.content);
}

class UpdateNoteEvent extends NoteEvent {
  final int index;
  final String content;

  const UpdateNoteEvent(this.index, this.content);
}

class EditNoteEvent extends NoteEvent {
  final int index;
  final String oldContent;

  const EditNoteEvent(this.index, this.oldContent);
}

class ConsumeAddingNoteEvent extends NoteEvent {
  const ConsumeAddingNoteEvent();
}

// Note State
@freezed
@immutable
class NoteState with _$NoteState {
  const factory NoteState(
      {@Default([]) List<Note> notes,
        @Default(null) int? selectedNoteIndex,
        @Default(false) bool loading,
        @Default(false) bool addingNote,
        @Default(consumed) EmptyStateEvent noteAdded}) = _NoteState;

  const NoteState._() : super();
}

@immutable
class Note extends Equatable {
  final String content;
  final bool loading;

  const Note(this.content, this.loading);

  @override
  List<Object?> get props => [content, loading];
}
