import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:note_app/bloc/note_bloc.dart';

class UpdateNoteScreen extends StatefulWidget {

  const UpdateNoteScreen({super.key});

  @override
  State<UpdateNoteScreen> createState() => _UpdateNoteScreenState();
}

class _UpdateNoteScreenState extends State<UpdateNoteScreen> {
  final TextEditingController _controller = TextEditingController();
  late final NoteBloc _noteBloc;

  @override
  void initState() {
    _noteBloc = Get.find<NoteBloc>(tag: 'noteBloc');
    assert(_noteBloc.state.selectedNoteIndex != null);
    _controller.text = _noteBloc.state.notes[_noteBloc.state.selectedNoteIndex!].content;
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Update Note')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: TextField(
          controller: _controller,
          maxLines: null,
          keyboardType: TextInputType.multiline,
          decoration: const InputDecoration(hintText: 'Enter your note here'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.save),
        onPressed: () {
          final content = _controller.text;
          _noteBloc.add(UpdateNoteEvent(_noteBloc.state.selectedNoteIndex!, content));
          Navigator.pop(context);
        },
      ),
    );
  }
}
