import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:note_app/bloc/note_bloc.dart';
import 'package:note_app/bloc/utils.dart';
// ... other imports

class AddNoteScreen extends StatefulWidget {
  const AddNoteScreen({super.key});

  @override
  State<AddNoteScreen> createState() => _AddNoteScreenState();
}

class _AddNoteScreenState extends State<AddNoteScreen> {
  final TextEditingController _controller = TextEditingController();
  late final NoteBloc _noteBloc;
  StreamSubscription<NoteState>? _streamSubscription;

  @override
  void initState() {
    _noteBloc = Get.find<NoteBloc>(tag: 'noteBloc');

    _streamSubscription = _noteBloc.stream.listen((state) {
      if (state.noteAdded is Triggered) {
        _noteBloc.add(const ConsumeAddingNoteEvent());
        Navigator.pop(context);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _streamSubscription?.cancel();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NoteBloc, NoteState>(
        bloc: _noteBloc,
        builder: (context, state) => Scaffold(
              appBar: AppBar(title: const Text('Edit Note')),
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextField(
                  controller: _controller,
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  decoration:
                      const InputDecoration(hintText: 'Enter your note here'),
                ),
              ),
              floatingActionButton: FloatingActionButton(
                child: _floatingButton(state),
                onPressed: () {
                  final content = _controller.text;
                  _noteBloc.add(AddNoteEvent(content));
                },
              ),
            ));
  }

  Widget _floatingButton(NoteState state) {
    if (state.addingNote) {
      return const SizedBox(
          width: 16,
          height: 16,
          child:
              CircularProgressIndicator(strokeWidth: 2.0, color: Colors.black));
    } else {
      return const Icon(Icons.save);
    }
  }
}
