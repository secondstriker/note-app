import 'package:flutter/material.dart';
import 'package:note_app/add_note_screen.dart';
import 'package:note_app/note_list_screen.dart';
import 'package:note_app/update_note_screen.dart';
// ... other imports

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      title: 'Flutter Notes BLoC',
      initialRoute: '/',
      routes: {
        '/': (_) => const NoteListScreen(),
        '/addNote': (_) => const AddNoteScreen(),
        '/updateNote': (_) => const UpdateNoteScreen(),
      },
    );
  }
}
