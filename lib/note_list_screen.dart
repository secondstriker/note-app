import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:note_app/bloc/note_bloc.dart';

class NoteListScreen extends StatefulWidget {
  const NoteListScreen({super.key});

  @override
  State<NoteListScreen> createState() => _NoteListScreenState();
}

class _NoteListScreenState extends State<NoteListScreen> {
  late final NoteBloc _noteBloc;

  @override
  void initState() {
    _noteBloc = NoteBloc();
    Get.put(_noteBloc, tag: 'noteBloc');
    _noteBloc.add(const LoadNotesEvent());
    super.initState();
  }

  @override
  void dispose() {
    Get.delete(tag: 'noteBloc');
    _noteBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _getNotes(context);
  }

  Scaffold _getNotes(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Notes')),
      body: BlocBuilder<NoteBloc, NoteState>(
          bloc: _noteBloc,
          builder: (context, state) {
            if (state.loading) {
              return const Center(child: CircularProgressIndicator());
            } else {
              return ListView.builder(
                itemCount: state.notes.length,
                itemBuilder: (context, index) {
                  return ListTile(
                      title: GestureDetector(
                    child: _getNoteItem(state.notes[index]),
                    onTap: () {
                      _noteBloc.add(
                          EditNoteEvent(index, state.notes[index].content));
                      Navigator.pushNamed(context, '/updateNote');
                    },
                  ));
                },
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, '/addNote');
        },
      ),
    );
  }

  Widget _getNoteItem(Note note) {
    if (note.loading) {
      return const Text("Updating ...");
    } else {
      return Text(note.content);
    }
  }
}
